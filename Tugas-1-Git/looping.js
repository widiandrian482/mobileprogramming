//1. LOOPING while
	
console.log('Looping Pertama')
	
var maju = 2;
	
while(maju <= 20){
	
    console.log(maju + ' - I Love Coding');
	
    maju +=2;
	
}
	
console.log("")
	

	
console.log('Looping Kedua')
	
var mundur = 20;
	
while(mundur >=2){
	
    console.log(mundur + ' - I will become a mobile developer');
	
    mundur -=2;
	
}
	
console.log("")
	

	
//2. Looping for
	
for(var nomor = 1; nomor <= 20; nomor ++) {
	
    if (nomor % 2 == 1 && nomor % 3 == 0){
	
        console.log(nomor + ' - I Love Coding');
	
    } else if(nomor % 2 == 1){
	
        console.log(nomor + ' - Teknik'); 
	
    }else{
	
        console.log(nomor + ' - Informatika');
	
    }
	
}
	
console.log("")
	

	
//3. Membuat Persegi Panjang
	
class Persegi{
	
    run(n,m){
	
        for(var i = 1; i <= n; i++) {
	
            var x = '';
	
            for(var j = 1; j <= m; j++){
	
                var x = x + '#';
	
            }
	
            console.log(x)
	
        }
	
    }
	
}
	
var persegi = new Persegi;
	
persegi.run(4,8);
	
console.log('')
	

	
//4. Membuat Tangga
	
class Tangga{
	
    run(n){
	
        for(var i = 1; i <= n; i++ ){
	
            var x = '';
	
        for (var j = 1; j <= i; j++){
	
            var x = x + '#';
	
        }
	
        console.log(x)
	
        }
	
    }
	
}
	
var tangga = new Tangga;
	
tangga.run(7,7);
	
console.log('')
	

	
//5. Membuat Catur
	
class Catur{
	
    run(n,m){
	
        for(var i = 1; i <= n; i++){
	
            if(i % 2 == 0){
	
                var x = '';
	
            } else {
	
                var x =' ';
	
            }
	
        for(var j = 1; j <= m; j++){
	
            if( j % 2 == 0){
	
                var x = x + ' ';
	
            }else{
	
                var x = x + '#';
	
            }
	
        }
	
        console.log(x)
	
        }
	
    }
	
}
	
var catur = new Catur;
	
catur.run(8,8);